extends "res://Person.gd"

export var hunger: int
export var speed: int
export var stamina: int
export var strength: int

func _ready():
	_init(hunger, speed, stamina, strength)
