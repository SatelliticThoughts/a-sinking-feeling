extends Node


class_name Person

var _hunger: int
var _speed: int
var _stamina: int
var _strength: int

var _curr_action: String

func _init(hunger: int, speed: int, stamina: int, strength: int):
	set_hunger(hunger)
	set_speed(speed)
	set_stamina(stamina)
	set_strength(strength)


func get_hunger() -> int:
	return _hunger

func set_hunger(hunger: int) -> void:
	self._hunger = hunger

func get_speed() -> int:
	return _speed

func set_speed(speed: int) -> void:
	self._speed = speed

func get_stamina() -> int:
	return _stamina

func set_stamina(stamina: int) -> void:
	self._stamina = stamina

func get_strength() -> int:
	return _strength

func set_strength(strength: int) -> void:
	self._strength = strength
